﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class UserController : ApiController
    {
        string strCon = ConfigurationManager.ConnectionStrings["ComStructure"].ConnectionString;

        // GET: api/User
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/User/5
        public ArrayList Get(string username,string password)
        {
            ArrayList memArr = new ArrayList();

            
            using (SqlConnection conn = new SqlConnection(strCon))
            {
                String sqlQuery = "select * from Users";
                SqlCommand cmd = new SqlCommand(sqlQuery, conn);
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Users usr = new Users();
                    usr.Username = rdr["Username"].ToString();
                    usr.Firstname = rdr["Firstname"].ToString();
                    usr.Lastname = rdr["Lastname"].ToString();
                    usr.Password = rdr["Password"].ToString();
                    memArr.Add(usr);
                }

                

                return memArr;

            }


            //var opts = new Dictionary<string, string>();
            //opts.Add("username", username);
            //opts.Add("password", password);
            

            //return opts;

        }

        // POST: api/User
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/User/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
        }
    }
}
